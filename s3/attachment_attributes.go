package s3

type AttachmentAttributes struct {
	FileSize    int64  `json:"fileSize"`
	FileName    string `json:"fileName"`
	FileKey     string `json:"fileKey"`
	ContentType string `json:"contentType"`
}
