package s3

import (
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

type S3Downloader struct{}

func (s S3Downloader) CopyFile(objectKey, pathTo string) error {
	sess, _ := buildS3Session()
	downloader := s3manager.NewDownloader(sess)

	file, err := os.Create(pathTo)
	if err != nil {
		return err
	}

	defer file.Close()

	_, err = downloader.Download(file,
		&s3.GetObjectInput{
			Bucket: aws.String(os.Getenv("S3_BUCKET_NAME")),
			Key:    aws.String(objectKey),
		})
	if err != nil {
		return err
	}
	return nil
}

func (s S3Downloader) GetFileContent(objectKey string) ([]byte, error) {
	sess, _ := buildS3Session()
	downloader := s3manager.NewDownloader(sess)

	buf := aws.NewWriteAtBuffer([]byte{})

	_, err := downloader.Download(buf,
		&s3.GetObjectInput{
			Bucket: aws.String(os.Getenv("S3_BUCKET_NAME")),
			Key:    aws.String(objectKey),
		},
	)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
