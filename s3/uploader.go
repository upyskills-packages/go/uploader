package s3

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/upyskills-packages/go/uploader/image"
)

type S3Uploader struct{}

// UploadFileFromURL download a file from given url and upload it to AWS S3
func (s S3Uploader) UploadFileFromURL(
	url string,
	key string,
	composeKeyWithFileName bool,
) (interface{}, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	buffer, _ := ioutil.ReadAll(response.Body)
	return uploadFromBlob(buffer, url, key, composeKeyWithFileName)
}

func (s S3Uploader) UploadFileFromBuffer(
	buffer []byte,
	contentType,
	key string,

) (interface{}, error) {
	size := len(buffer)

	err := putS3Object(key, buffer, int64(size), contentType)
	if err != nil {
		return nil, err
	}

	return AttachmentAttributes{
		FileSize:    int64(size),
		FileKey:     key,
		ContentType: contentType,
	}, nil
}

func (s S3Uploader) UploadFileFromDisc(filePath string, key string) (interface{}, error) {
	buffer, _ := ioutil.ReadFile(filePath)
	return uploadFromBlob(buffer, filePath, key, true)
}

func (s S3Uploader) UploadFileFromFileHeader(
	fileHeader *multipart.FileHeader,
	key string,
	dimensions *map[string]image.Dimensions,
	maxsize int,
) (interface{}, error) {

	file, err := fileHeader.Open()
	if err != nil {
		return "", errors.New("file not found")
	}
	defer file.Close()

	size := fileHeader.Size
	buffer := make([]byte, size)
	contentType := fileHeader.Header.Get("Content-Type")
	fileName := strings.ToLower(fileHeader.Filename)

	_, err = file.Read(buffer)
	if err != nil {
		return "", err
	}

	// upload other sizes
	if strings.Contains(strings.ToLower(contentType), "pdf") {
		buffer, _ = image.ConvertPdfToImage(buffer)
		fileName = strings.ReplaceAll(fileName, "pdf", image.PDF_TO_IMAGE_TYPE)
		size = 0
		contentType = fmt.Sprintf("image/%s", image.PDF_TO_IMAGE_TYPE)
	}

	// Upload original file size
	objectKey := buildKeyWithSize(key, "original", fileName)
	if err = putS3Object(objectKey, buffer, size, contentType); err != nil {
		return nil, err
	}

	// Upload other sizes if configurated
	if dimensions != nil {
		for k, value := range *dimensions {
			objectKey := buildKeyWithSize(key, k, fileName)
			if resizedBuffer, nize, err := image.Resize(buffer, value); err == nil {
				if err = putS3Object(objectKey, resizedBuffer, int64(nize), contentType); err != nil {
					return nil, err
				}
			} else {
				return nil, err
			}
		}
	}

	return AttachmentAttributes{
		FileSize:    int64(size),
		FileName:    fileName,
		FileKey:     buildKeyWithSize(key, "-", fileName),
		ContentType: contentType,
	}, nil
}

// GetSignURL return the sign url for s3 objects
func (s S3Uploader) GetSignImageURLByField(field *postgres.Jsonb, size string, defaultUrl string) string {
	var signedURL string
	if field.RawMessage != nil {
		var attachment = AttachmentAttributes{}
		json.Unmarshal(field.RawMessage, &attachment)
		signedURL = s.GetSignImageURL(attachment.FileKey, size)
	} else if defaultUrl != "" {
		signedURL = defaultUrl
	}
	return signedURL
}

// GetSignURL return the sign url for s3 objects
func (s S3Uploader) GetSignImageURL(objectKey, size string) string {
	return s.GetSignFile(getFormattedKey(objectKey, size))
}

func (s S3Uploader) GetSignFile(objectKey string) string {
	sess, _ := buildS3Session()
	svc := s3.New(sess)

	req, _ := svc.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(os.Getenv("S3_BUCKET_NAME")),
		Key:    aws.String(objectKey),
	})
	urlStr, err := req.Presign(15 * time.Minute)

	if err != nil {
		log.Println("Failed to sign request", err)
	}
	return urlStr
}

func uploadFromBlob(buffer []byte, path, key string, composeKeyWithFileName bool) (interface{}, error) {
	size := len(buffer)
	contentType := http.DetectContentType(buffer)
	fileName := buildFileNameFromPath(path, contentType)

	objectKey := key
	if composeKeyWithFileName {
		objectKey = buildKeyWithSize(key, "", fileName)
	}

	err := putS3Object(objectKey, buffer, int64(size), contentType)
	if err != nil {
		return nil, err
	}

	return AttachmentAttributes{
		FileSize:    int64(size),
		FileName:    fileName,
		FileKey:     objectKey,
		ContentType: contentType,
	}, nil
}

func putS3Object(objectKey string, buffer []byte, size int64, contentType string) error {
	s, err := buildS3Session()
	if err != nil {
		return errors.New("Could not start a session")
	}

	_, err = s3.New(s).PutObject(&s3.PutObjectInput{
		Bucket:             aws.String(os.Getenv("S3_BUCKET_NAME")),
		Key:                aws.String(objectKey),
		ACL:                aws.String("private"),
		Body:               bytes.NewReader(buffer),
		ContentLength:      aws.Int64(int64(size)),
		ContentType:        aws.String(contentType),
		ContentDisposition: aws.String("attachment"),
	})
	return err
}

func buildFileNameFromPath(path, contentType string) string {
	segments := strings.Split(path, "/")
	fileName := segments[len(segments)-1]

	// Linkedin image URL not return a filename in path
	if strings.Contains(fileName, "?") && strings.Contains(fileName, "&") {
		fileName = fmt.Sprintf("%s.%s", uuid.NewString(), getFileExtension(contentType))
	}
	return fileName
}

func getFileExtension(contentType string) string {
	arr := strings.Split(contentType, "/")
	return arr[1]
}

func buildKeyWithSize(key, size, fileName string) string {
	if strings.TrimSpace(size) == "" {
		size = "original"
	} else if strings.TrimSpace(size) == "-" {
		return key + "/" + fileName
	}
	return strings.ReplaceAll(key, ":size", size) + "/" + fileName
}

func getFormattedKey(key, size string) string {
	if strings.TrimSpace(size) == "" {
		size = "original"
	}
	return strings.ReplaceAll(key, ":size", size)
}
