package image

// https://www.imagemagick.org/script/command-line-processing.php#geometry

import (
	"encoding/binary"

	"gopkg.in/gographics/imagick.v3/imagick"
)

type Dimensions struct {
	Width            uint
	Height           uint
	GeometryOperator string
}

const PDF_TO_IMAGE_TYPE string = "jpeg"

func Resize(blob []byte, dimensions Dimensions) ([]byte, uint, error) {
	imagick.Initialize()
	defer imagick.Terminate()

	mw := imagick.NewMagickWand()
	defer mw.Destroy()
	if err := mw.ReadImageBlob(blob); err != nil {
		return nil, 0, err
	}

	dWidth, dHeight := getSize(mw, dimensions)
	if err := mw.ResizeImage(dWidth, dHeight, imagick.FILTER_LANCZOS); err != nil {
		return nil, 0, err
	}

	if err := mw.SetImageCompressionQuality(95); err != nil {
		return nil, 0, err
	}

	img := mw.GetImageBlob()
	length := uint(binary.Size(img))
	return img, length, nil
}

func ConvertPdfToImage(blob []byte) ([]byte, error) {
	imagick.Initialize()
	defer imagick.Terminate()

	mw := imagick.NewMagickWand()
	defer mw.Destroy()

	if err := mw.ReadImageBlob(blob); err != nil {
		return nil, err
	}

	mw.SetIteratorIndex(0) // This being the page offset
	mw.SetFormat("jpeg")
	return mw.GetImageBlob(), nil
}

func getSize(mw *imagick.MagickWand, dimensions Dimensions) (width uint, height uint) {
	width = dimensions.Width
	height = dimensions.Height
	if dimensions.GeometryOperator == "" {
		return
	}

	iWidth := mw.GetImageWidth()
	iHeight := mw.GetImageHeight()

	if iWidth > iHeight && dimensions.GeometryOperator == ">" {
		height = (uint(width) * uint(iHeight)) / uint(iWidth)
	}
	return
}
