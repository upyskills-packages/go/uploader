package uploader

import (
	"gitlab.com/upyskills-packages/go/uploader/s3"
)

type Downloader interface {
	CopyFile(objectKey, pathTo string) error
	GetFileContent(objectKey string) ([]byte, error)
}

func NewDownloader() Downloader {
	// provider := os.Getenv("STORAGE_PROVIDER")
	// if provider == "aws" {
	// 	return s3.S3{}
	// }
	// return nil
	return s3.S3Downloader{}
}
