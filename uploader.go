package uploader

import (
	"mime/multipart"

	"github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/upyskills-packages/go/uploader/image"
	"gitlab.com/upyskills-packages/go/uploader/s3"
)

type Uploader interface {
	UploadFileFromURL(url string, key string, composeKeyWithFileName bool) (interface{}, error)
	UploadFileFromDisc(filePath string, key string) (interface{}, error)
	UploadFileFromFileHeader(
		fileHeader *multipart.FileHeader,
		key string,
		dimensions *map[string]image.Dimensions,
		maxsize int,
	) (interface{}, error)
	UploadFileFromBuffer(
		buffer []byte,
		contentType,
		key string,
	) (interface{}, error)
	GetSignImageURLByField(field *postgres.Jsonb, size string, defaultUrl string) string
	GetSignImageURL(objectKey, size string) string
	GetSignFile(objectKey string) string
}

func NewUploader() Uploader {
	// provider := os.Getenv("STORAGE_PROVIDER")
	// if provider == "aws" {
	// 	return s3.S3{}
	// }
	// return nil
	return s3.S3Uploader{}
}
