module gitlab.com/upyskills-packages/go/uploader

go 1.16

require (
	github.com/aws/aws-sdk-go v1.44.14
	github.com/google/uuid v1.3.0
	github.com/jinzhu/gorm v1.9.16
	gopkg.in/gographics/imagick.v3 v3.4.0
)
